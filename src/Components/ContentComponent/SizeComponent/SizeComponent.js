import { Component } from "react";
import pizzaHawaii from "../../../assets/Images/hawaiian.jpg"
import pizzaBacon from"../../../assets/Images/bacon.jpg"
import pizzaSeafood from "../../../assets/Images/seafood.jpg"

class Size extends Component{
    render(){
        return (
            <div>
                <div className="card-header text-center ">
                    <h3 >CHỌN LOẠI PIZZA</h3>
                    <hr className="line"/>
                </div>
                <div className="div-pizza-select">
                    <div className="card div-card-pizza">
                        <div className="card-header img-header">
                            <img className="div-img" src ={pizzaSeafood} alt="pizza seafood"/>
                        </div>
                        <div className="card-body" >
                            <h3>OCEAN MANIA</h3>
                            <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                            <p>Xốt cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh cua, Hành tây</p>
                        </div>
                            <button className="btn btn-warning  btn-choose" id="btn-large">Chọn</button>
                    </div>
                    <div className="card div-card-pizza">
                        <div className="card-header img-header" >
                            <img className="div-img" src ={pizzaHawaii} alt="pizza hawaii"/>
                        </div>
                        <div className="card-body " >
                            <h3>HAWAIIAN</h3>
                            <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                            <p>Xốt cà chua, Phô mai Mozzarella, Thịt dăm bông, Thơm</p>
                        </div>
                            <button className="btn btn-warning  btn-choose" id="btn-large">Chọn</button>
                    </div>
                    <div className="card div-card-pizza">
                        <div className="card-header img-header" >
                            <img className="div-img" src ={pizzaBacon} alt="pizza bacon"/>
                        </div>
                        <div className="card-body " >
                            <h3>OCEAN MANIA</h3>
                            <p>PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                            <p>Xốt cà chua, Phô mai Mozzarella, Tôm, Mực, Thanh cua, Hành tây</p>
                        </div>
                            <button className="btn btn-warning  btn-choose" id="btn-large">Chọn</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Size;