import { Component } from "react";

class Drink extends Component {
    render(){
        return (
            <div>
                <div className="card-header text-center ">
                    <h3 >CHỌN ĐỒ UỐNG</h3>
                    <hr className="line"/>
                </div>
                <div className="card select-drink">
                <select className="form-control" >
                    <option>Chọn nước uống</option>
                    <option>Coca Cola</option>
                    <option>Pepsi</option>
                    <option>Aquafina</option>
                    <option>Smootie</option>
                    <option>Dừa Tắc</option>  
                </select>
                </div>
            </div>
        )
    }
}
export default Drink;