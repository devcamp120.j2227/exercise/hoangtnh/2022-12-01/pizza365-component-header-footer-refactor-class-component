import { Component } from "react";

class Form extends Component{
    render(){
        return(
            <div>
                <div className="card-header text-center ">
                    <h3 >GỬI ĐƠN HÀNG</h3>
                    <hr className="line"/>
                </div>
                <div className="card user-infor ">
                    <div className="row user-order">
                        <label>Tên</label>
                        <input className="form-control" placeholder="Họ Tên"></input>
                    </div>
                    <div className="row user-order">
                        <label>Email</label>
                        <input className="form-control"  placeholder="Email"></input>
                    </div>
                    <div className="row user-order">
                        <label>Điện thoại</label>
                        <input className="form-control"  placeholder="Điện Thoại"></input>
                    </div>
                    <div className="row user-order">
                        <label>Địa chỉ</label>
                        <input className="form-control"  placeholder="Địa chỉ"></input>
                    </div>
                    <div className="row user-order">
                        <label>Mã giảm giá</label>
                        <input className="form-control"  placeholder="Mã giảm giá"></input>
                    </div>
                    <div className="row user-order">
                        <label>Ghi chú</label>
                        <input className="form-control"  placeholder="Ghi chú"></input>
                    </div>
                    <div className="row user-order">
                        <button className="btn btn-warning btn-choose">Gửi</button>
                    </div>
                </div>
            </div>
        )
    }
}
export default Form;