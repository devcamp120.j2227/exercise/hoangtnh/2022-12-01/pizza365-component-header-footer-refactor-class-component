import React, { Component } from "react";
import Introduce from "./IntroduceComponent/IntroduceComponent";
import Type from "./TypeComponent/TypeComponent";
import Size from "./SizeComponent/SizeComponent";
import Drink from "./DrinkComponent/DrinkComponent";
import Form from "./FormComponent/FormComponent";
class Content extends Component{
    render(){
        return(
            <div className="container">
                <Introduce/>
                <Type/>
                <Size/>
                <Drink/>
                <Form/>
            </div>
        )
    }
}
export default Content;