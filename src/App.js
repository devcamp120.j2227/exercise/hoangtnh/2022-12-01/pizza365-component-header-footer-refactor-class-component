import "bootstrap/dist/css/bootstrap.min.css"; //import bootstrap
import './App.css';
import HeaderComponent from "./Components/HeaderComponent/HeaderComponent";
import FooterComponent from "./Components/FooterComponent/FooterComponent";
import Content from "./Components/ContentComponent/ContentComponent";


function App() {
  return (
    <div>
      <HeaderComponent/>
      <Content/>
      <FooterComponent/>
    </div>
  );
}

export default App;
